/*12.   Пошаговая игра без боев между игроками. Аналогична предыдущему варианту, но игроки,
        встретившиеся на одной ячейке, выводят сообщения, сумма жизней меняется у того,
        кто занял ячейку первым. Игрок, пройдя игровое поле, выводит свой результат.      */
#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <stdbool.h>
#include <time.h>


#define SIDE_LEFT 1
#define SIDE_TOP 2
#define  TO_RIGHT 21
#define  TO_DOWN 22

#define clear() printf("\033[H\033[J")

struct struct_field {
    int** matrix;
    int size;
};
typedef struct struct_field type_field;

struct struct_coordinates {
    int x;
    int y;
};
typedef struct struct_coordinates type_coordinates;

struct struct_player {
    type_coordinates* coordinates;
    int side;
    int life;
    int player_number;
};
typedef struct struct_player type_player;

void one_step_to_the_right(type_player* player, type_field* field) {
    player->coordinates->x++;
}

void one_step_to_the_down(type_player* player, type_field* field) {
    player->coordinates->y++;
}

int randomize () {
    int value;
	value = rand() % 2;
	if (value == 1) {
		value = rand() % 2;
		if (value == 1) {
			value = rand() % 5;
		}
		if (value == 0) {
			value = -(rand() % 5);
		}
	}
    return  value;
}

type_field* create_field (int size) {
    srand((unsigned)time(NULL));
    type_field* field = (type_field*)malloc(sizeof(type_field));
    field->matrix = (int**)malloc(sizeof(int*) * size);
    field->size = size;
    for (int i = 0; i < size; i++) {
		field->matrix[i] = (int*)malloc(sizeof(int) * size);
		for (int j = 0; j < size; j++) {
			field->matrix[i][j] = randomize();
		}
	}
    return field;
}

void set_zero (type_player* players, type_field* field) {
    for (int i = 0; i < field->size; i++) {
        for (int j = 0; j < field->size; j++) {
            if ((players->coordinates->x == j) && (players->coordinates->y == i)) {
                field->matrix[i][j] =0;
                }
            }
        }
    }


bool check_player (type_player* players, type_field* field, int y_rows, int x_cols) {
    for (int k = 0; k < players->player_number; k++) {
        if ((players[k].coordinates->y == y_rows) && (players[k].coordinates->x == x_cols)) {
            return true;
        }
    }
    return false;
}

void add_life_player (type_player* players, type_field* field) {
    players->life =players->life + field->matrix[players->coordinates->y][players->coordinates->x];
}

void print_field (type_field* field, type_player* players) {
    for (int i = 0; field->size > i; i++) {
        for (int j = 0; j < field->size; j++) {
            printf("----");
        }
        printf("\n");
        for (int j = 0; j < field->size; j++) {
            if (check_player(players, field, i, j)) {
                printf("| %c|", '*');
            }
            else if (field->matrix[i][j] >= 0)
                printf("| %d|", field->matrix[i][j]);
            else
                printf("|%d|", field->matrix[i][j]);
		}
        printf("\n");
	}
    printf("\n");
}

type_player* set_players (int player_number) {
    type_player* players = (type_player*)malloc(sizeof(type_player) * player_number);
    for (int i = 0; i < player_number; i++) {
        players[i].coordinates = (type_coordinates*)malloc(sizeof(type_coordinates));
    }
    players->coordinates = (type_coordinates*)malloc(sizeof(type_coordinates));
    int buf_side;
    int buf_start_point;
    for (int i = 0; i < player_number; i++) {
        players[i].player_number = player_number;
        players[i].life = 0;
        printf("PLAYER [%d] Choose your SIDE\nLeft -- 1; Top -- 2\n", i);
        scanf("%d", &buf_side);
        if (buf_side == 1) {
            players[i].side = SIDE_LEFT;
            printf("PLAYER [%d] Choose your START POINT\n[1..n]\n", i);
            scanf("%d", &buf_start_point);
            players[i].coordinates->x = 0;
            players[i].coordinates->y = buf_start_point-1;
        }
        if (buf_side == 2) {
            players[i].side = SIDE_TOP;
            printf("PLAYER [%d] Choose your START POINT\n[1..n]\n", i);
            scanf("%d", &buf_start_point);
            players[i].coordinates->x = buf_start_point-1;
            players[i].coordinates->y = 0;
        }
    }
    return players;
}

void server(int player_number, type_field* field, type_player* players) {
    int pid[player_number], status, stat;
    int direction = 0;
    for (int j = 0; j < field->size; j++) {
        for (int i = 0; i < player_number; i++) {
            pid[i] = fork();
            if (-1 == pid[i]) {
                perror("fork");
                exit(1);
            } else if (0 == pid[i]) {
                if (players[i].side == SIDE_TOP) {
                    direction = TO_DOWN;
                    exit(direction);
                }
                if (players[i].side == SIDE_LEFT) {
                    direction = TO_RIGHT;
                    exit(direction);
                }
            }
        }
        for (int i = 0; i < player_number; i++) {
            status = waitpid(pid[i], &stat, 0);
            if (pid[i] == status) {
                if (WEXITSTATUS(stat) == TO_DOWN) {
                    add_life_player(&players[i], field);
                    set_zero(&players[i], field);
                    one_step_to_the_down(&players[i], field);

                }
                if (WEXITSTATUS(stat) == TO_RIGHT) {
                    add_life_player(&players[i], field);
                    set_zero(&players[i], field);
                    one_step_to_the_right(&players[i], field);

                }
                clear();
                print_field(field, players);
                sleep(1);
            }
        }
    }
}

void print_score(type_player* players) {
    int max_life = -100;
    int win_player_number;
    for (int k = 0; k < players->player_number; k++) {
        printf("\nPLAYER [%d] life ---> %d\n", k , players[k].life);
        if (players[k].life > max_life) {
            max_life = players[k].life;
            win_player_number = k;
        }

    }
    printf("\nWINNER IS %d player!!! Congratulations! Your score is %d\n", win_player_number, max_life);
}

int main(int argc, char* argv[]) {
    int size, players_number;
    printf("Enter size of matrix\n");
    scanf("%d", &size);
    printf("Enter number of players\n");
    scanf("%d", &players_number);

    type_field* my_field = create_field(size);
    type_player* players = set_players(players_number);

    server(players_number, my_field, players);

    print_score(players);

    return 0;
}
