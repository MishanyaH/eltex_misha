#!/bin/bash

CFLAGS="-I/home/miha/bin/include -I/home/miha/bin/glib-2.0/include -I/home/miha/bin/lib/libffi-3.2.1/include" \
LDFLAGS="-L/home/miha/bin/lib " \
PKG_CONFIG_PATH="/home/miha/bin/lib/pkgconfig" \
CC="arm-linux-gnueabi-gcc" \
./configure \
--with-screen=ncurses \
--host=arm-linux-gnueabi \
--prefix=/home/miha/bin \
--with-ncurses-libs=/home/miha/bin/lib \
--with-ncurses-includes=/home/miha/bin/include
make
make install