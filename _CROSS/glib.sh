#!/bin/bash
echo "glib_cv_stack_grows=no" > config.cache
echo "glib_cv_uscore=no" >> config.cache
echo "ac_cv_func_posix_getpwuid_r=yes" >> config.cache
echo "ac_cv_func_posix_getgrgid_r=yes" >> config.cache
CC="arm-linux-gnueabi-gcc" \
CPPFLAGS="-I/home/miha/bin/include -I/home/miha/bin/lib/libffi-3.2.1/include" \
LDFLAGS="-L/home/miha/bin/lib" \
LIBFFI_LIBS="-L/home/miha/bin/lib -lffi" \
LIBFFI_CFLAGS="-I/home/miha/bin/lib/libffi-3.2.1/include" \
./configure --cache-file=config.cache --host=arm-linux-gnueabi --prefix=/home/miha/bin -C --enable-static --enable-shared=yes