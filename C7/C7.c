/*2. Умножение матрицы на вектор.
 *Обработка одной строки матрицы - в порожденном процессе. */

#include <stdio.h>
#include <malloc.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <memory.h>
#include <fcntl.h>
#include <wait.h>
#include <time.h>
#include <sys/time.h>
//Структура матрицы
struct matrix {
    int** matrix;
    int size;
};
typedef struct matrix type_matrix;
//Структура вектора
struct vector {
    int* vector;
    int size;
};
typedef struct vector type_vector;
//Для каждого процесса -- свой FIFO
char* new_name(const char* input_name, char* format) {
    char* new_name = (char*)malloc(sizeof(char)*30);
    strcat(new_name, input_name);
    strcat(new_name, format);
    strcat(new_name, "\0");
    return new_name;
}
//Создаем FIFO
char* create_FIFO(int j) {
    char *fifo_name;
    char *fifo_template_name = "/tmp/fifo";
    char *buf_for_cut = (char *) malloc(sizeof(char) * 30);
    sprintf(buf_for_cut, "%d", j);
    fifo_name = new_name(fifo_template_name, buf_for_cut);
    if (mkfifo(fifo_name, 0666) < 0) {
        perror("mkfifo: ");
    }
    return fifo_name;
}
//Создаем матрицу
type_matrix* create_matrix (int size) {
    type_matrix* matrix = (type_matrix*)malloc(sizeof(type_matrix));
    matrix->matrix = (int**)malloc(sizeof(int*) * size);
    matrix->size = size;
    for (int j = 0; j < size; j++) {
        matrix->matrix[j] = (int *) malloc(sizeof(int) * size);
    }
    return matrix;
}
//Создаем вектор
type_vector* create_vector (int size) {
    type_vector* vector= (type_vector*)malloc(sizeof(type_vector));
    vector->size = size;
    vector->vector = (int *) malloc(sizeof(int) * size);
    return vector;
}
//Матрица -- рандом
void set_matrix (type_matrix* matrix) {
    srand((unsigned)time(NULL));
    for (int i = 0; i < matrix->size; i++) {
        for (int j = 0; j < matrix->size; j++) {
            matrix->matrix[i][j] = rand() % 10;
            printf("%d ", matrix->matrix[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}
//Вектор -- рандом
void set_vector (type_vector* vector) {
    for (int j = 0; j < vector->size; j++) {
        vector->vector[j] = rand() % 10;
        printf("%d\n", vector->vector[j]);
    }
    printf("\n");
}
//Перемножение одной строчки матрицы на вектор, затем суммирование
int multiplication (type_matrix* matrix, type_vector* vector, int number_row_of_matrix) {
    int result_part = 0;
    for (int i = 0; i < matrix->size; ++i) {
        result_part = result_part + matrix->matrix[number_row_of_matrix][i] * vector->vector[i];
    }
    return result_part;
}
//Функция сервер-клиент
int server (type_matrix* matrix, type_vector* vector) {
    int readfd;                     //дескриптор для чтения главным процессом
    int stat, status = 0;           //для waitpid
    int result_part_buffer;         //результат multiplication
    char *fifo_files[matrix->size]; //названия всех FIFO
    pid_t pid[matrix->size];        //PID процесса

    for (int i = 0; i < matrix->size; i++) {
        fifo_files[i] = create_FIFO(i); //создаем все FIFO
        pid[i] = fork();                //создаем процесс
        if (-1 == pid[i]) {
            perror("fork: ");
            return 1;
        } else if (0 == pid[i]) {
            int writefd;                                //дескриптор для записи порожденным процессом
            writefd = open(fifo_files[i], O_WRONLY);    //открыть FIFO для записи
            result_part_buffer = multiplication(matrix, vector, i);          //вычислить промежуточный результат
            write(writefd, &result_part_buffer, sizeof(result_part_buffer)); //записать его
            close(writefd);
            exit(0);
        }
    }
    //далее главный процесс
    for (int i = 0; i < matrix->size; i++) { //открываем все FIFO для чтения
        if ((readfd = open(fifo_files[i], O_RDONLY, 0)) <= 0) {
            perror("open to read: ");
            return 1;
        }
        read(readfd, &result_part_buffer, sizeof(result_part_buffer)); //читаем результат
        matrix->matrix[0][i] = result_part_buffer;     //записываем его в 0 строчку
        close(readfd);                                 //закрываем FIFO
        unlink(fifo_files[i]);                         //отключаем FIFO
    }
    for (int i = 0; i < matrix->size; i++) { //ожидаем завершения всех процессов для выявления ошибок
        status = waitpid(pid[i], &stat, 0);
        if (pid[i] == status) {
            if (WEXITSTATUS(stat) != 0) {
                perror("process failed: ");
                exit(1);
            }
        }
    }
}//всё
//Вывести результат на экран
void print_result (type_matrix* matrix) {
    for (int i = 0; i < matrix->size; i++) {
        printf("%d\n", matrix->matrix[0][i]);
    }
}

void free_matrix (type_matrix* matrix) {
    for (int j = 0; j < matrix->size; j++) {
        free(matrix->matrix[j]);
    }
    free(matrix->matrix);
    free(matrix);
}

void free_vector (type_vector* vector) {
    free(vector->vector);
    free(vector);
}

void free_space(type_matrix* matrix, type_vector* vector) {
    free_matrix(matrix);
    free_vector(vector);
}

int main() {
    type_matrix* matrix = create_matrix(4);
    type_vector* vector = create_vector(4);
    set_matrix(matrix);
    set_vector(vector);
    server(matrix, vector);
    print_result(matrix);
    free_space(matrix, vector);
    return 0;
}