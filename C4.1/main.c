#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

/*  Исключить строки с длиной, больше заданной
    Параметры командной строки:
	1. Имя входного файла
	2. Заданная длина строки    */

//функция для создания имени выходного файла
char* newName(const char* input_name, char* format) {
    char* new_name = (char*)malloc(sizeof(char)*30);
    size_t c = strchr(input_name, '.') - input_name;
    strncpy(new_name, input_name, c);
    strcat(new_name, format);
    strcat(new_name, "\0");
    return new_name;
}
//создание выходного файла
void deleteWords (char* input_name, int max_length) {
    FILE* input_file;   //входной файл
    FILE* output_file;  //выходной файл
    char* p_string;     //читаемая строка (указатель)
    char string[100];   //читаемая строка
    char* word;         //читаемое слово
    char* format = {".myfile"};     //формат выходного файла
    char* output_name = newName(input_name, format);

    input_file = fopen(input_name, "r");
    if (input_file == NULL) {
        printf("Error opening file");
        getchar();
        exit(1);
    }
    output_file = fopen(output_name, "w+");
    if (output_file == NULL) {
        printf("Error opening file");
        getchar();
        exit(1);
    }
    while(1) {
        p_string = fgets(string, sizeof(string), input_file);
        if (p_string == NULL)
        {
            if ( feof (input_file) != 0)
            {
                printf ("\nDone reading\n");
                break;
            }
            else
            {
                printf ("\nError reading\n");
                break;
            }
        }
        word = strtok (string," ");
        if (strlen(word) < max_length) {
            fprintf(output_file,"%s ", word);
        }
        while (word != NULL)
        {
            if (strlen(word) < max_length) {
                fprintf(output_file,"%s ", word);
            }
            word = strtok (NULL, " ");
        }
    }
    fclose(input_file);
    fclose(output_file);
}

int main(int argc, char** argv) {
    int max_length = atoi(argv[2]);
    if(argc < 2) {
        printf("Wrong arguments\n");
    }
    deleteWords(argv[1], max_length);
    return 0;
}
