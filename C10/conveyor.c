/*        Бригадный подряд. Родительский процесс создает заданное количество дочерних (условных рабочих), которые выкладывают
        на конвейер (в файл, глобальный массив) деталь (например, свой идентификатор) если на нем есть место. После
        запуска рабочих, запускается дочерний процесс-контролер, который снимает с конвейера деталь для проверки
        (есть ли такой идентификатор в списке процессов). Если рабочий не может выложить деталь за число тактов, равное числу
        рабочих (нет места на конвейере),
        он прерывает работу с жалобой на контролера. Если контролер не может взять с конвейера
        ни одной детали, он прерывает работу с жалобой на бригаду.
*/
#include <pthread.h>

#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/types.h>
#include "stack_array.h"
#include "conveyor.h"

TypeDetails details = {.mutex = PTHREAD_MUTEX_INITIALIZER};

void* worker (void* args) {

    int count_attempt = 0;
    int thread_exit_code = TREAD_EXIT_SUCCES;
    pthread_mutex_t local_mutex;

    unsigned id = (unsigned)pthread_self() % 100;

    for (;;) {

        sleep(1);
        pthread_mutex_lock(&local_mutex);
        if (isFullStackArray(&details.stack_array)) {
            for (int i = 0; i < NUM_TREAD; ++i) {
                sleep(1);
                if (!isFullStackArray(&details.stack_array)) {
                    break;
                }
                if (i == (NUM_TREAD -1)) {
                    thread_exit_code = TREAD_EXIT_ERROR;
                    printf("Controller finished with code %d", thread_exit_code);
                    pthread_exit(&thread_exit_code);
                }
            }
        }
        pthread_mutex_unlock(&local_mutex);

        pthread_mutex_lock(&details.mutex);
        putStackArray(&details.stack_array, id);
        pthread_mutex_unlock(&details.mutex);

        printf("Thread put [%u]\n", id);

        sleep(4);
    }
}

void* controller (void* args) {

    int get_value;
    int count_attempt = 0;
    pthread_mutex_t local_mutex;
    int thread_exit_code = TREAD_EXIT_SUCCES;

    for (;;) {

        sleep(1);
        pthread_mutex_lock(&local_mutex);
        if (isEmptyStackArray(&details.stack_array)) {
            for (int i = 0; i < NUM_TREAD; ++i) {
                sleep(1);
                if (!isEmptyStackArray(&details.stack_array)) {
                    break;
                }
                if (i == (NUM_TREAD -1)) {
                    thread_exit_code = TREAD_EXIT_ERROR;
                    printf("Controller finished with code %d", thread_exit_code);
                    pthread_exit(&thread_exit_code);
                }
            }
        }
        pthread_mutex_unlock(&local_mutex);

        pthread_mutex_lock(&details.mutex);
        get_value = getStackArray(&details.stack_array);
        pthread_mutex_unlock(&details.mutex);

        printf("Controller get value [%u]\n", get_value);

        sleep(1);
    }
}

void createWorker (pthread_t* thread) {
    int status;
    for (int i = 0; i < NUM_TREAD; i++) {
        sleep(1);
        status = pthread_create(&thread[i], NULL, worker, NULL);
        pthread_detach(thread[i]);
        if(status != 0) {
            perror("thread worker");
        }
    }
}

void createController (pthread_t* thread) {
    int status;
    status = pthread_create(thread, NULL, controller, NULL);
    pthread_detach(*thread);
    if(status != 0) {
        perror("thread controller");
    }
}

int main () {
    pthread_t worker_thread[NUM_TREAD];
    pthread_t controller_thread;

    createWorker(worker_thread);
    createController(&controller_thread);

    getchar();
    return 0;
}

