/****************************************/
/*			Стек на массиве				*/
/****************************************/

#include <stdio.h>
#include "stack_array.h"

/*Переменная ошибок*/
extern int errorStackArray;
extern const int okStackArray = 0;
extern const int fullStackArray = 1;
extern const int emptyStackArray = 10000;

/*Инициализация стека*/
void initStackArray(StackArray *S) {
    S->ptr = 0;
    errorStackArray = okStackArray;
}

/*Включение в стек*/
void putStackArray(StackArray *S, stackArrayBaseType E) {

    /*Если стек переполнен*/
    if (isFullStackArray(S)) {
        return;
    }
    /*Иначе*/
    S->buf[S->ptr] = E;
    S->ptr++;

}

/*Исключение из стека*/
stackArrayBaseType getStackArray(StackArray *S) {

    /*Если стек пуст*/
    if (isEmptyStackArray(S)) {
        return 10000;
    }
    S->ptr--;
    return S->buf[S->ptr];
}

/*Предикат: полон ли стек*/
int isFullStackArray(StackArray *S) {
    if (S->ptr == SIZE_STACK_ARRAY) {
        errorStackArray = fullStackArray;
        return 1;
    }
    return 0;
}

/*Предикат: пуст ли стек*/
int isEmptyStackArray(StackArray *S) {
    if (S->ptr == 0) {
        errorStackArray = emptyStackArray;
        return 1;
    }
    return 0;
}