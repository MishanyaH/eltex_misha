/****************************************/
/*			Стек на массиве				*/
/****************************************/

#ifndef _STACK_ARRAY_H_
#define _STACK_ARRAY_H_

/*Размер стека*/
#define SIZE_STACK_ARRAY 100

/*Описание исключительных ситуаций*/
const int okStackArray;
const int fullStackArray;
const int emptyStackArray;
/*********************************/

/*Переменная ошибок*/
int errorStackArray;

/*Базовый тип стека*/
typedef unsigned stackArrayBaseType;

/*Дескриптор стека*/
typedef struct {
    stackArrayBaseType buf[SIZE_STACK_ARRAY];
    unsigned ptr;
} StackArray;
/******************/

/*Функции работы со стеком*/
void initStackArray(StackArray *S);
void putStackArray(StackArray *S, stackArrayBaseType E);
stackArrayBaseType getStackArray(StackArray *S);
int isFullStackArray(StackArray *S);
int isEmptyStackArray(StackArray *S);
/**************************/

#endif