//
// Created by miha on 10.04.17.
//

#ifndef C10_CONVEYOR_H
#define C10_CONVEYOR_H


#define NUM_TREAD 4
#define TREAD_EXIT_ERROR 1;
#define TREAD_EXIT_SUCCES 2;

struct TypeDetails {
    pthread_mutex_t mutex;
    StackArray stack_array;
};
typedef struct TypeDetails TypeDetails;

void* worker (void* args);
void* controller (void* args);
void createWorker (pthread_t* thread);
void createController (pthread_t* thread);

#endif //C10_CONVEYOR_H
