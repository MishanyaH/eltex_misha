#!/bin/bash

read_config(){
	if [ -f ~/.config/backup_config.conf ]
	then
		user=`sed -n 1p ~/.config/backup_config.conf`
		first_path=`sed -n 2p ~/.config/backup_config.conf` #архивируемая папка
		backup_path=`sed -n 3p ~/.config/backup_config.conf` #куда 
		period=`sed -n 4p ~/.config/backup_config.conf`
	else
		user=""
		first_path=""
		backup_path=""
		period=""
		sudo touch ~/.config/backup_config.conf
		sudo chmod o+w ~/.config/backup_config.conf
	fi
}

copy () {
	echo backup_path $backup_path
	echo first_path $first_path
	time_name=$(date +%H'-'%M'_'%d'-'%m'-'%y)
	cd $backup_path
	tar -cf $time_name.zip $first_path
}

deletes () 
{
 if ! [ -d ~/my_cron ]; then
  mkdir ~/my_cron  
 fi 
 if ! [ -f ~/my_cron/create_crontab ]; then
  touch ~/my_cron/create_crontab 
 fi 
 if ! [ -f ~/my_cron/buffer_control ]; then
  touch ~/my_cron/buffer_backup  
 fi 
 my_cron=~/my_cron
 create_crontab=~/my_cron/create_crontab
 buffer_backup=~/my_cron/buffer_backup
 first_data="SHELL=/bin/sh\nPATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin\nDISPLAY=:0"
 echo -e $first_data > $create_crontab
 crontab $create_crontab
}

insert_config(){
	user=$(whiptail --title "settings" --inputbox "enter username" 10 60 $user 3>&1 1>&2 2>&3)
	first_path=$(whiptail --title "settings" --inputbox "select path for copy" 10 60 $first_path 3>&1 1>&2 2>&3)
	backup_path=$(whiptail --title "settings" --inputbox "select path for backup" 10 60 $backup_path 3>&1 1>&2 2>&3)
	period=$(whiptail --title "settings" --inputbox "periodic for backup in hours" 10 60 $period 3>&1 1>&2 2>&3)
	echo -e "$user\n$first_path\n$backup_path\n$period" > ~/.config/backup_config.conf
	echo "* */$period * * * bash ~/ELTEX/BASH/bash8.sh start" > $buffer_backup
	cat $buffer_backup >> $create_crontab 
	crontab create_crontab
}	

read_config
case $1 in
	start)
		copy
		;;
	set)
		insert_config
		;;
	stop)
		deletes
		;;	
	*) 
		echo "Use (start | stop | set)"
		;;
esac	
