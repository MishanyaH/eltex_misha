#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

struct  employers {
    char surname [25];
    int year_birth;
    int salary;
    };
typedef struct employers emplo ;

void readEmployers (emplo* employer) {
    printf("please, enter surname\n");
    scanf("%s", employer->surname);
    printf("please, enter year of birth\n");
    scanf("%d", &employer->year_birth);
    printf("please, enter salary\n");
    scanf("%d", &employer->salary);
}

void printEmployers (emplo** employers, int count) {
    for(int i = 0; i < count; i++) {
        printf("surname of employer %s\n", employers[i]->surname);
    }
}

static int cmp (const void* arg1, const void* arg2) {
    emplo* employer1 = (emplo*) arg1;
    emplo* employer2 = (emplo*) arg2;
    return ((*employer1).year_birth - (*employer2).year_birth);
}

int main() {
    int count = 3;
    printf("enter number of employers\n");
    scanf("%d", &count);
    emplo** employers = (emplo**)malloc(sizeof(emplo**) * count);
    for (int i = 0; i < count; i++) {
        employers[i] = (emplo*)malloc(sizeof(emplo));
        readEmployers(employers[i]);
    }
    qsort(employers, count, sizeof(emplo*), cmp);
    printEmployers(employers, count);
    for (int i = 0; i < count; i++)
    {
        free(employers[i]);
    }
    free(employers);
    return 0;
}

