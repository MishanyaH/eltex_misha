#include<stdio.h> //printf
#include<sys/socket.h>    //socket
#include<arpa/inet.h> //inet_addr
#include <unistd.h>
#include <memory.h>
#include "socket.h"

int* make_result (int* matrix, int* vector) {
    static int result[SOCKET_ARRAY_SIZE] = {0};

    int j = 0;
    for (int i = 0; i < LENGTH_OF_MATRIX; i++) {
        result[j] = result[j] + matrix[i] * vector[j];
        if (((i+1) % 10) == 0) {
            j++;
        }
    }

    for (int i = 0; i < LENGTH_OF_VECTOR; i++) {
        printf("%d ", result[i]);
    }
    return result;
}

int client () {
    int sock;

    struct sockaddr_in server;
    static int matrix[SOCKET_ARRAY_SIZE];
    static int vector[SOCKET_ARRAY_SIZE];
    int* result;

    sock = socket(AF_INET , SOCK_STREAM , 0);
    if (sock == -1)
    {
        printf("Could not create socket");
    }
    puts("Socket created");

    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_family = AF_INET;
    server.sin_port = htons( 8884 );

    if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
    {
        perror("connect failed. Error");
        return 1;
    }

    puts("Connected\n");

    if (read(sock, &matrix, sizeof(matrix)) < 0) {
        puts("recv failed");
    }
    puts("matrix read");

    if (read(sock, &vector, sizeof(vector)) < 0) {
        puts("recv failed");
    }
    puts("vector read");

    int buffer_matrix[SOCKET_ARRAY_SIZE];
    memcpy(buffer_matrix, matrix, SOCKET_ARRAY_SIZE * sizeof(int));
    int buffer_vector[SOCKET_ARRAY_SIZE];
    memcpy(buffer_vector, vector, SOCKET_ARRAY_SIZE * sizeof(int));

    result = make_result(buffer_matrix, buffer_vector);
    int buffer_result[SOCKET_ARRAY_SIZE];
    memcpy(buffer_result, result, SOCKET_ARRAY_SIZE * sizeof(int));

    if( send(sock , buffer_result, sizeof(buffer_result) , 0) < 0)
    {
        puts("Send failed");
        return 1;
    }
    puts("message send");

    close(sock);
    return 0;
}

int main(int argc , char *argv[])
{
    client();
}
