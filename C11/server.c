#include<stdio.h>
#include<sys/socket.h>
#include<arpa/inet.h> //inet_addr
#include<unistd.h>    //write
#include <stdlib.h>

int server () {
    int length = 10;
    static int matrix[100];
    static int vector[100];
    static int result[100];

    for (int i = 0; i < length*length; i++) {
        matrix[i] = rand() % 10;
    }
    for (int i = 0; i < 100; i++) {
        if ((i % 10) == 0) {
            printf("\n");
        }
        printf("%d ", matrix[i]);
    }
    printf("\n\n");

    for (int i = 0; i < length; i++) {
        vector[i] = rand() % 10;
    }
    for (int i = 0; i < 10; i++) {
        printf("%d ", vector[i]);
    }
    printf("\n\n");

    int socket_desc , client_sock , c , read_size;
    struct sockaddr_in server , client;

    socket_desc = socket(AF_INET , SOCK_STREAM , 0);
    if (socket_desc == -1)
    {
        printf("Could not create socket");
    }
    puts("Socket created");

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( 8884);

    if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
    {
        perror("bind failed. Error");
        return 1;
    }
    puts("bind done");

    listen(socket_desc , 3);

    puts("Waiting for incoming connections...");
    c = sizeof(struct sockaddr_in);

    client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
    if (client_sock < 0)
    {
        perror("accept failed");
        return 1;
    }
    puts("Connection accepted");

    if (write(client_sock , matrix , sizeof(matrix)) < 0) {
        puts("write failed");
    }
    puts("matrix send");

    if (write(client_sock , vector , sizeof(vector)) < 0) {
        puts("write failed");
    }
    puts("vector send");

    if (read(client_sock, result, sizeof(result)) < 0) {
        puts("recv failed");
    }
    puts("message recieved");

    for (int i = 0; i < 10; ++i) {
        printf("%d ", result[i]);
    }

    return 0;
}

int main(int argc , char *argv[])
{
    server();
}
