#!/bin/bash

#Написать скрипт для работы с инструментарием git. 
#Скрипт должен иметь функции: создания, редактирования и удаления
#(репо, веток, файлов, комитов), отправки и получения с удаленного репо
#(bitbucket.org или github.com). 

#Все работает, код прозрачный и понятный ;)
menu () 
{	
	echo
	echo "what do you want?? -___-"
	echo
	
	echo "[1] actions with snapshots"
	echo "[2] connect with external repository"
	echo "[3] actions with files"	
	echo "[4] push and pull to external repository"	
	
	read answer
	case "$answer" in
	"") echo "enter [1][2][3][4]"; 
	exit 65;;
	"1") commit_editing 
	;;
	"2") repo_connecting 
	;; 
	"3") files_editing
	;;
	"4") server_working 
	;;
	esac
}

init ()
{	
	echo "enter path of repo"
	read path_name
	cd $path_name
	git init
	echo "repo is initilated" 
	menu
}

commit_editing () 
{	
	status ()
	{	
		echo "your status:"
		git status
	}
	add () 
	{
		echo "enter name of file"
		read name_file
		git add $name_file 
	}
	commit ()
	{	
		echo "enter comment for commit"
		read comment
		git commit -m "$comment"
	}
	edit_commit ()
	{	
		echo "enter comment for commit"
		read comment
		git commit --amend -m "$comment"
	}
	
	echo
	echo "[0] status"
	echo "[1] add changes"
	echo "[2] commit changes"	
	echo "[3] edit commit"	
	
	read answer	
	case "$answer" in
	"") echo "enter [1][2][3][4]"; 
	exit 65;;
	"0") status
	;;
	"1") add
	;;
	"2") commit
	;; 
	"3") edit_commit
	;;
	esac
	menu	
}

repo_connecting () 
{	
	echo
	echo "[1] show repos"
	echo "[2] add repos"	
	echo "[3] rename repos"	
	echo "[4] delete repos"
	
	read answer	
	case "$answer" in
	"") echo "enter [1][2][3][4]"; 
	exit 65;;
	"1") git remote
	;;
	"2") echo "enter URL of repo"
		 read repo_url
		 echo "enter name of repo"
		 read repo_name
		 git remote add $repo_name $repo_url
		 echo "connecting completed!"
	;; 
	"3") echo "enter new name"
		 read new_repo_name
		 git remote rename $repo_name $new_repo_name 
		 repo_name = $new_repo_name
	;;
	"4") echo "are you... sure?? [y][anykey]"
		 if [[ $answer1  == "y" ]]
		 then
		 git remote rm $repo_name
		 echo "repo is deleted :("
		 else 
		 echo "right choice :)" 
		 fi
	;;
	esac
	menu
}

files_editing ()
{	
	echo
	echo "[1] make file"
	echo "[2] edit file"	
	echo "[3] delete file"	
	
	read answer
	case "$answer" in
	"") echo "enter [1][2][3]"; 
	exit 65;;
	"1") echo "enter new file name"
		 read file_name
		 touch $file_name
	;;
	"2") echo "enter new file name"
		 read file_name
		 nano $file_name
	;; 
	"3") echo "delete new file name"
		 read file_name
		 rm $file_name
	;;
	esac
	menu
}	

branch_editing ()
{	
	echo
	echo "[1] show your branches"
	echo "[2] create branche"	
	echo "[3] go to branch"
	echo "[4] merge branches"	
	
	read answer
	case "$answer" in
	"") echo "enter [1][2][3][4]"; 
	exit 65;;
	"1") git branch
	;;
	"2") echo "enter branch name to create"
		 read branch_name
		 git branch $branch_name
	;; 
	"3") echo "enter branch name to go"
		 read branch_name
		 git checkout $branch_name
	;;
	"4") echo "enter branch name to merge"
		 read branch_name
		 git merge $branch_name
	esac
	menu
}

server_working ()
{	
	echo
	echo "[1] push to repo"
	echo "[2] pull from repo"
	
	read answer
	case "$answer" in
	"") echo "enter [1][2]"; 
	exit 65;;
	"1") echo "enter repo name to push"
		 read repo_to_push
		 echo "enter branch name to push"
		 read branch_to_push
		 git push $repo_to_push $branch_to_push
	;;
	"2") echo "enter repo name to pull"
		 read repo_to_pull
		 echo "enter branch name to pull"
		 read branch_to_pull
		 git pull $repo_to_pull $branch_to_pull
	;; 
	esac
	menu
}	

init
menu
