#include <stdio.h>
#include <dlfcn.h>

int main(int argc, char* argv[])
{
    void *ext_library;
    double value=0;
    int (*math)(int a, int b);
    ext_library = dlopen ("/home/miha/ELTEX/C5/prog_with_makefile/DL/libresult.so",RTLD_LAZY);
    if (!ext_library){
        fprintf(stderr,"dlopen() error: %s\n", dlerror()); return 1; };
    math = dlsym(ext_library, argv[1]);
    int a=3.0;
    int b=14;
    printf("%d\n", (*math)(a, b));
    dlclose(ext_library);
}