#!/bin/bash

#Написать скрипт, который контролирует выполнение какого-либо процесса 
#и в случае прерывания этого процесса возобновляет его работу.
#Скрипт должен запросить периодичность проверки и имя процесса. 
#Скрипт должен иметь функции: создания, редактирования и удаления 
#заданий контроля. Сторонние задания cron, скрипт должен игнорировать
#(не показывать). Для выполнения задания НЕ разрешается создавать 
#скрипты (скрипт должен запускаемый из крона и скрипт запускаемый 
#пользователем должен быть один.).

#ОНА РАБОТАЕТ!!!!! ЯХУУУУУУУУУУУУУУУУУ
#В общем, все по науке, задания для будильника и контролера записываются в отдельные файлы и потом подгружаются
#в отдельный файл из которого образуется крон. Можно записывать, менять, удалять, при этом будильник не будет 
#изменен. Так должна была выглядеть вторая лаба ;) 

set_data () 
{
 if ! [ -d ~/my_cron ]; then
  mkdir ~/my_cron  
 fi 
 if ! [ -f ~/my_cron/create_crontab ]; then
  touch ~/my_cron/create_crontab 
 fi 
 if ! [ -f ~/my_cron/buffer_control ]; then
  touch ~/my_cron/buffer_control  
 fi 
 if ! [ -f ~/my_cron/buffer_clock ]; then
  touch ~/my_cron/buffer_clock  
 fi 
 my_cron=~/my_cron
 create_crontab=~/my_cron/create_crontab
 buffer_control=~/my_cron/buffer_control
 buffer_clock=~/my_cron/buffer_clock
 first_data="SHELL=/bin/sh\nPATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin\nDISPLAY=:0"
 echo -e $first_data > $create_crontab
}

sets () 
{ 
 echo "enter periodicity of execution checks [minutes]"
 read periodicity
 echo "enter name of program"
 read name_program
 echo "*/$periodicity * * * *   if ! pgrep "$name_program"; then "$name_program"; fi" >> $buffer_control
 cat $buffer_control >> $create_crontab
 cat $buffer_clock >> $create_crontab
 echo >> $create_crontab
 crontab $create_crontab
 echo "seting completed" 
 return 0
}

remakes ()
{
 echo "Do you want to remake control? :y:anykey "
 read answer1
 if [[ $answer1  == "y" ]]; then
  echo "choose the line number to remake"
  sed = $buffer_control | sed 'N;s/\n/\t/'
  read answer2
  sed -i "${answer2}d" $buffer_control  
  sets
  echo "remaking completed"
  exit 1
 else
  echo "exit program"
  exit 2
 fi
}

deletes ()
{
 echo "Do you want to delete clock? :y:anykey "
 read answer1
 if [[ $answer1  == "y" ]]; then
  echo "choose the line number delete"
  sed = $buffer_control | sed 'N;s/\n/\t/'
  read answer2
  sed -i "${answer2}d" $buffer_control  
  cat $buffer_control >> $create_crontab
  cat $buffer_clock >> $create_crontab
  echo >> $create_crontab
  crontab $create_crontab
  echo "deleting completed"  
  exit 1 
 else
  echo "exit program"
  exit 2
 fi
}  

name="$(whoami)"
echo "Hello $name"

set_data

case "$1" in
"") echo "Using: ${0##*/} -s [set] | -r [remake] | -d [delete] "; exit 65
;;
-\r) remakes
;;   
-\s) sets
;;
-\d) deletes
;;
     
esac
