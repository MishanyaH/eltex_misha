/*  5. Винни-Пух и пчелы. Заданное количество пчел добывают мед равными порциями, задерживаясь в пути
 *     на случайное время. Винни-Пух потребляет мед порциями заданной величины за заданное время и столько
 *     же времени может прожить без питания. Работа каждой пчелы реализуется в порожденном процессе.
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <ctype.h>
#include <time.h>

#define MAX_LEN 1024

//Vinny set
#define VINNY_START_POT_SIZE 10
#define VINNY_TIME_LIVE_AND_EAT 10
#define VINNY_FOOD_TO_EAT 15
//Bees set
#define BEES_FOOD_TO_BRING 1
#define PROCESS_BEE_NUMBER 5

union semun {
    int val;                  /* значение для SETVAL */
    struct semid_ds *buf;     /* буферы для  IPC_STAT, IPC_SET */
    unsigned short *array;    /* массивы для GETALL, SETALL */
    /* часть, особенная для Linux: */
    struct seminfo *__buf;    /* буфер для IPC_INFO */
};

struct shared_space {
    int vinny_size_of_pot;
    int vinny_time_count;
    int vinny_end;

    int bees_time_to_travel[PROCESS_BEE_NUMBER];
    int bees_time_count[PROCESS_BEE_NUMBER];
};
typedef struct shared_space type_space;

type_space *create_space() {
    static type_space space;
    for (int i = 0; i < PROCESS_BEE_NUMBER; ++i) {
        space.bees_time_to_travel[i]= 0;
        space.bees_time_count[i] = 0;
    }
    space.vinny_size_of_pot = VINNY_START_POT_SIZE;
    space.vinny_time_count = 0;
    space.vinny_end = 0;
    return &space;
}

void Vinny_Puh() {

    srand((unsigned) time(NULL));

    type_space* space = create_space();

    pid_t pid[PROCESS_BEE_NUMBER];
    pid_t wpid;
    int status = 0;

    int shmid;
    key_t key;

    int semid;
    union semun arg;
    struct sembuf lock_res = {0, -1, 0};    //блокировка ресурса
    struct sembuf rel_res = {0, 1, 0};    //освобождение ресурса

    FILE *fptr;
    fptr = fopen("/tmp/ftok", "rb+");
    if(fptr == NULL) //if file does not exist, create it
    {
        fptr = fopen("/tmp/ftok", "wb");
    }

    /* Получим ключ, Один и тот же ключ можно использовать как
    для семафора, так и для разделяемой памяти */
    if ((key = ftok("/tmp/ftok", 'S')) < 0) {
        printf("Невозможно получить ключn");
        exit(1);
    }
    fclose(fptr);

    /* Создадим семафор - для синхронизации работы с разделяемой памятью.*/
    semid = semget(key, 1, 0666 | IPC_CREAT);

    /* Установить в семафоре № 0 (Контроллер ресурса) значение "1" */
    arg.val = 1;
    semctl(semid, 0, SETVAL, arg);

    /* Создадим область разделяемой памяти */
    if ((shmid = shmget(key, sizeof(type_space), IPC_CREAT | 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

    for (int i = 0; i < PROCESS_BEE_NUMBER; i++) {
        {
            pid[i] = fork();
            if (0 == pid[i]) {
                /* Получим доступ к разделяемой памяти */
                if ((space = (type_space*)shmat(shmid, NULL, 0)) == (type_space *) -1) {
                    perror("shmat");
                    exit(1);
                }
                if ((semop(semid, &lock_res, 1)) == -1) {
                    perror("semop");
                    exit(1);
                }
                space->bees_time_to_travel[i]= 1 + rand() % 4;
                /* Освободим разделяемую память */
                if ((semop(semid, &rel_res, 1)) == -1) {
                    perror("semop");
                    exit(1);
                }
                while (1) {
                    printf("vinny size of pot: %d\n", space->vinny_size_of_pot);
                    if (space->bees_time_to_travel[i]> 0) {
                        while (1) {
                            sleep(1);
                            space->bees_time_to_travel[i]--;
                            if (space->bees_time_to_travel[i] < 0) {
                                break;
                            }
                        }
                    }
                    if ((semop(semid, &lock_res, 1)) == -1) {
                        perror("semop");
                        exit(1);
                    }
                    space->vinny_size_of_pot= space->vinny_size_of_pot + BEES_FOOD_TO_BRING;
                    space->bees_time_to_travel[i] = 1 + rand() % 4;
                    /* Освободим разделяемую память */
                    if ((semop(semid, &rel_res, 1)) == -1) {
                        perror("semop");
                        exit(1);
                    }
                    /* Заблокируем разделяемую память */
                    if (space->vinny_end == 1) {
                        break;
                    }

                }
                /* Отключимся от разделяемой памяти */
                if (shmdt(space) < 0) {
                    perror("shmdt");
                    exit(1);
                }
                exit(0);
            } else if (pid[i] < 0) {
                perror("fork"); /* произошла ошибка */
                exit(1); /*выход из родительского процесса*/
            }
        }
    }
    /* Получим доступ к разделяемой памяти */
    if ((space = (type_space *) shmat(shmid, NULL, 0)) == (type_space *) -1) {
        perror("shmat");
        exit(1);
    }
    while (1) {
        if ((space->vinny_size_of_pot < 0) ||  (space->vinny_end  = 0)) {
            while (1) {
                sleep(1);
                if (space->vinny_size_of_pot < 0) {
                    space->vinny_time_count++;
                }
                if (space->vinny_time_count > VINNY_TIME_LIVE_AND_EAT) {
                    space->vinny_end  = 1;
                    space->vinny_time_count = 0;
                    break;
                }
            }
        }
        else if ((space->vinny_size_of_pot >= 0) || (space->vinny_end  = 0)){
            while (1) {
                sleep(1);
                if (space->vinny_size_of_pot > 0) {
                    space->vinny_time_count++;
                }
                if (space->vinny_time_count > VINNY_TIME_LIVE_AND_EAT) {
                    space->vinny_size_of_pot = space->vinny_size_of_pot - VINNY_FOOD_TO_EAT;
                    space->vinny_time_count = 0;
                    break;
                }
            }
        }
        if (space->vinny_end == 1) {
            break;
        }
    }
    if (shmdt(space) < 0) {
        perror("shmdt");
        exit(1);
    }
    /* Удалим созданные объекты IPC */
    if (shmctl(shmid, IPC_RMID, 0) < 0) {
        perror("shmctl");
        exit(1);
    }
    if (semctl(semid, 0, IPC_RMID) < 0) {
        perror("semctl");
        exit(1);
    }
    printf("Vinny is dead :(");
}

int main(int argc, char **argv){
    Vinny_Puh();
    return 0;
}
