#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x38947cfb, "module_layout" },
	{ 0x985558a1, "printk" },
	{ 0xa8d1d322, "remove_proc_entry" },
	{ 0xc973ad9d, "create_proc_entry" },
	{ 0x94d32a88, "__tracepoint_module_get" },
	{ 0xf0fdf6cb, "__stack_chk_fail" },
	{ 0xc3aaf0a9, "__put_user_1" },
	{ 0x3c2c5af5, "sprintf" },
	{ 0x167e7f9d, "__get_user_1" },
	{ 0xa1c76e0a, "_cond_resched" },
	{ 0x2fa29d01, "module_put" },
	{ 0xb4390f9a, "mcount" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "80D7DFE9DB6D5AFE8660104");
