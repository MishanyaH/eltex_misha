#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 1024 

int cstring_cmp(const void *a, const void *b)
{
    const char **ia = (const char **)a;
    const char **ib = (const char **)b;
    return strcmp(*ia, *ib);
}

char** readMas(int count){
	char buffer[MAX_LEN];
	char **mas; 
	mas = (char **)malloc(sizeof(char *)*count);
    for (int i = 0; i < count ; i++){
        scanf("%s", buffer); 
        mas[i] = (char *)malloc(sizeof(char)*strlen(buffer)); 
        strcpy(mas[i], buffer); 
    }
    return mas; 
}

void printMas(char **mas, int count){
    for (int i = 0; i < count ; i++){
        printf("%s\n", mas[i]);
    }
}

void freeMas(char **mas, int count){
	for (int i = 0; i < count; i++){
        free(mas[i]); 
    }
    free(mas);
}

 int main()
{
    char **mas = NULL;
	int count = 3;
	mas = readMas(count);
	printMas(mas, count);
	qsort(mas, count, sizeof(char *), cstring_cmp);
	printMas(mas, count);
	freeMas(mas, count);
    return 0;
}
