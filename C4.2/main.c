#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

/*  Удалить из текста заданный символ
    Параметры командной строки:
	1. Имя входного файла
	2. Заданный символ    */

//функция для создания имени выходного файла
//создание выходного файла
void deleteLetters (char* input_name, char letter) {
    FILE *input_file;   //входной файл
    char ch;
    int size = 100;
    int i = 0;
    char* buffer = (char*)malloc(sizeof(char)*size);

    input_file = fopen(input_name, "r");
    if (input_file == NULL) {
        printf("Error opening file");
        getchar();
        exit(1);
    }

    while(!feof(input_file)) {
        ch = (char) fgetc(input_file);
        if (ch == letter) {
            continue;
        }
        else {
            buffer[i] = ch;
            i++;
            if (i > size) {
                size *= 2;
                buffer = (char*)realloc(buffer, sizeof(char)*size);
            }
        }
    }
    buffer[i] = '\0';

    freopen(input_name, "w", input_file);
    if (input_file == NULL) {
        printf("Error opening file");
        getchar();
        exit(1);
    }

    for (int j = 0; buffer[j] != '\0'; j++) {
        fprintf(input_file, "%c", buffer[j]);
    }

    free(buffer);
    fclose(input_file);
}

int main(int argc, char** argv) {
    if(argc < 2) {
        printf("Wrong arguments\n");
    }
    deleteLetters(argv[1], *argv[2]);
    return 0;
}
