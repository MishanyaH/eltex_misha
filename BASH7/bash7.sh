#!/bin/bash

set_static_rules(){
	sudo ufw allow http
	sudo ufw allow https
	sudo ufw allow ssh
	sudo ufw allow ftp
	sudo ufw allow 3306
}

scan(){
	netstat -anltp | grep "LISTEN" | awk '{print($4)}' | sed 's|.*:::||' | sed 's|.*:||' > temp
}

set_rules(){
	touch temp
	act="allow"
	i=1
	var=""
	while [ $i -le $(sed -n '$=' temp) ]
	do
		if test $act="allow"
			then
			echo -e "\n"
			echo "allow port $(sed -n ${i}p temp)"
			sudo ufw allow $(sed -n ${i}p temp)
		else
			echo -e "\n"
			echo "deny port $(sed ${i}p temp)"
			sudo ufw deny $(sed ${i}p temp)
		fi		
		let i=i+1
	done
	rm temp
}

clear_rules(){
	sudo ufw reset
}

show_info(){
	sudo ufw status verbose
}

case $1 in
	show)
	show_info
	;;
	set)
	set_static_rules
	;;
	clear)
	clear_rules
	;;
	deny)
	scan
	act="deny"
	set_rules
	;;
	allow)
	scan
	act="allow"
	set_rules
	;;
	*)
	echo "using: show static clear deny allow"
esac